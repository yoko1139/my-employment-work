//=============================================================================
//
// 矢印のモデル処理 [arrow.h]
// Author :横田航大
//
//=============================================================================
#pragma once

//*****************************************************************************
// マクロ定義
//*****************************************************************************

#define MAX_ARROW		(1)			// 矢印の数
#define	POS_X_ARROW		(30.0f)		// カメラの初期位置(X座標)
#define	POS_Y_ARROW		(0.0f)		// カメラの初期位置(Y座標)
#define	POS_Z_ARROW		(-5.0f)		// カメラの初期位置(Z座標)

struct ARROW
{
	D3DXVECTOR3			pos;				// モデルの位置
	D3DXVECTOR3			rot;				// モデルの向き(回転)
	D3DXVECTOR3			scl;				// モデルの大きさ(スケール)
	D3DXMATRIX			mtxRoll;			// 回転
	LPDIRECT3DTEXTURE9	pD3DTexture;		// テクスチャへのポインタ
	LPD3DXMESH			pD3DXMesh;			// メッシュ情報へのポインタ
	LPD3DXBUFFER		pD3DXBuffMat;		// マテリアル情報へのポインタ
	DWORD				nNumMat;			// マテリアル情報の数
	D3DXMATRIX			mtxWorld;			// ワールドマトリックス
	float				Radius;				// 矢印の半径
	float				BaseAngle;			// 矢印の角度
	bool				use;				//使っているか

};

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
HRESULT InitArrow(int type);
void UninitArrow(void);
void UpdateArrow(void);
void DrawArrow(void);
ARROW *GetArrow();
